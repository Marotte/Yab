# YAB

[Version française](README_FR.md)

YAB (or Yab) is a tool created to optimize communication between people, promote team collaboration and agile working. Thus, it increases the overall productivity of your organization.

It can be described as a crossover web application between a chatroom and a microblog. It also has a white board, because sometime a quick sketch has more meaning than a thousand words…

Yab is modern state-of-the-art software. It depends on Python3, SQLite, [uWSGI](https://uwsgi-docs.readthedocs.io/en/latest/) and Nginx.

The client code is pure Javascript, it makes use of no external library/framework like AngularJS, Node.js, jQuery, etc… The server code should work with Python ≥ 3.4 and uWSGI ≥ 2.0.


## Installation

You **must** modify/create three files to setup a working instance of Yab:

### The uWSGI INI file: _yab.ini_
 
This file is usually to be placed in _/etc/uwsgi/apps-available_ with a symlink in _/etc/uwsgi/apps-enabled_ to activate it.

You must at least modify the 'chdir' parameter of the provided file to suit your needs.

### The virtual host Nginx file.
 
_yab.nginx.snippet_ is not a complete vhost file but may help you configure one by yourself.

### The Javascript file _yab.js_
 
As it contains the adress of the websocket server, you must change the 'host' variable (and optionally the 'ws' variable, depending on how you’ve configured Nginx)

    var host = 'yab.example.com'
    var ws = new WebSocket('wss://'+host+'/ws')

those are the first two lines of the script.

## Personalization

YAB offers only very few possibilities of customization… you may modify the following files:

### yab.css

To change some client settings, like colors.

### index.html

For page’s title or page’s keywords.

## Demonstration

There an instance running at [oxyure.com](https://board.oxyure.com). Feel free to experiment but don’t forget it’s still a beta version and a good number of functionalities are not implemented yet. Obviously have never been, and probably never will.

The software is perfect as is. All the shity design here and there are a feature. A feature to remain us all that this is in our imperfections that stand our humanness. 
