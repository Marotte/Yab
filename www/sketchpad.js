// https://zipso.net/a-simple-touchscreen-sketchpad-using-javascript-and-html5/

function getCookie(cname) {
    var name = cname + "="
    var ca = document.cookie.split(';')
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i]
        while (c.charAt(0) == ' ') {
            c = c.substring(1)
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length)
        }
    }
    return ""
}

var canvas,ctx
var mouseX,mouseY,mouseDown=0,mouseDrag=0
var omouseX, omouseY
var color     = getCookie('yab_sketchpad_color') 
var size      = getCookie('yab_sketchpad_pensize')
var alpha     = getCookie('yab_sketchpad_alpha')
var drawtype  = getCookie('yab_sketchpad_drawtype')


if (!size) { size = '3' }
if (!color) { color = '#000000' }
if (!alpha) { alpha = '1' }
if (!drawtype) { drawtype = 'dot' }

function autoCropCanvas(ctx, canvas) {

var w = canvas.width,
h = canvas.height,
pix = {x:[], y:[]},
imageData = ctx.getImageData(0,0,canvas.width,canvas.height),
x, y, index

for (y = 0; y < h; y++) {
    for (x = 0; x < w; x++) {
        index = (y * w + x) * 4
        if (imageData.data[index+3] > 0) {

            pix.x.push(x)
            pix.y.push(y)

        }   
    }
}
pix.x.sort(function(a,b){return a-b})
pix.y.sort(function(a,b){return a-b})
var n = pix.x.length-1;

w = pix.x[n] - pix.x[0]
h = pix.y[n] - pix.y[0]
var cut = ctx.getImageData(pix.x[0], pix.y[0], w, h)

canvas.width = w
canvas.height = h
ctx.putImageData(cut, 0, 0)

return [canvas.toDataURL(),w,h]

}

function drawDot(ctx,x,y,size,color,alpha) {
    
    ctx.globalAlpha = alpha
    
    if (document.getElementById('connect-dots').checked && mouseDrag === 1) {
        
        ctx.beginPath()
        ctx.moveTo(omouseX, omouseY)
        ctx.lineWidth=size*2
        ctx.lineTo(x,y)
        ctx.strokeStyle=color
        ctx.stroke()
    }
    
    else {

        ctx.fillStyle = color
        ctx.beginPath()
        ctx.arc(x, y, size, 0, Math.PI*2, true)
        ctx.closePath()
        ctx.fill()
    }
    
    omouseX = x
    omouseY = y
}

function drawLine(ctx,x,y,size,color,alpha) {

    ctx.globalAlpha = alpha

    ctx.beginPath()
    ctx.moveTo(omouseX, omouseY)
    ctx.lineWidth=size*2
    ctx.lineTo(x,y)
    ctx.strokeStyle=color
    ctx.stroke()

    omouseX = x
    omouseY = y
}

function drawRect(ctx,x,y,size,color,alpha) {

    ctx.globalAlpha = alpha

    ctx.beginPath()
    ctx.lineWidth=size*2
    ctx.rect(omouseX,omouseY,x-omouseX,y-omouseY)
    ctx.strokeStyle=color
    ctx.stroke()

    omouseX = x
    omouseY = y
}

function clearCanvas(canvas,ctx) {
    ctx.clearRect(0, 0, canvas.width, canvas.height)
}

function sketchpad_mouseDown(e) {

    mouseDown=1
    drawtype = document.getElementById('draw-type').value
    
    if (drawtype == 'dot') { drawDot(ctx,mouseX,mouseY,size,color,alpha) }
    
    else if (drawtype == 'line') { omouseX = mouseX; omouseY = mouseY; }
    
    else if (drawtype == 'rectangle') { omouseX = mouseX; omouseY = mouseY; }
    
    mouseDrag=1
}

function sketchpad_mouseUp(e) {

    getMousePos(e)
    
    if (e.target.getAttribute('id') != 'sketchpad') { return null; }
    
    mouseDown=0
    mouseDrag=0

    if (drawtype == 'line') { drawLine(ctx,mouseX,mouseY,size,color,alpha) }
    else if (drawtype == 'rectangle') { drawRect(ctx,mouseX,mouseY,size,color,alpha) }
    
    omouseX = mouseX
    omouseY = mouseY

}

function sketchpad_mouseMove(e) { 

    getMousePos(e)

    if (e.target.getAttribute('id') != 'sketchpad') { return null; }

    if (mouseDown==1 && drawtype == 'dot') {
        drawDot(ctx,mouseX,mouseY,size,color,alpha)
    }
}

function getMousePos(e) {
    
    if (!e)
        var e = event

    if (e.offsetX) {
        mouseX = e.offsetX
        mouseY = e.offsetY
    }
    else if (e.layerX) {
        mouseX = e.layerX
        mouseY = e.layerY
    }
}

function sketchpadInit() {

    canvas = document.getElementById('sketchpad')
    e_color = document.getElementById('color-picker')
    e_size = document.getElementById('size-picker')
    e_alpha = document.getElementById('alpha-picker')
    e_drawtype = document.getElementById('draw-type')

    e_color.value = color
    e_size.value = size
    e_alpha.value = alpha
    e_drawtype.value = drawtype

    e_color
        .addEventListener('change', function (e) {
        color = e_color.value
        document.cookie = 'yab_sketchpad_color='+color
        })
        
    e_size
        .addEventListener('change', function (e) {
        size = e_size.value
        document.cookie = 'yab_sketchpad_pensize='+size
        })
    
    e_alpha
        .addEventListener('change', function (e) {
        alpha = e_alpha.value
        document.cookie = 'yab_sketchpad_alpha='+alpha
        })
        
    e_drawtype
        .addEventListener('change', function (e) {
        drawtype = e_drawtype.value
        document.cookie = 'yab_sketchpad_drawtype='+drawtype
        })
    
    if (canvas.getContext)
        ctx = canvas.getContext('2d');

    if (ctx) {

        canvas.addEventListener('mousedown', sketchpad_mouseDown, false)
        window.addEventListener('mousemove', sketchpad_mouseMove, false)
        window.addEventListener('mouseup', sketchpad_mouseUp, false)
        
        document.getElementById('clear-sketchpad').addEventListener('click', function (e) {
            
            clearCanvas(canvas, ctx)  
        })

    }
}

function isBlank(canvas) {
    
    var blank = document.createElement('canvas')
    blank.width = canvas.width
    blank.height = canvas.height
    return canvas.toDataURL() == blank.toDataURL()
}

function getBase64PNGImage () {
    
    cropped = autoCropCanvas(ctx, canvas)
    img_src = cropped[0]
    img_width = cropped[1]
    img_height = cropped[2]
    img_alt = document.getElementById('drawing-title').value
    if (isBlank(canvas)) { return '' }
    ret = '<img class="drawing" width="'+img_width+'" height="'+img_height+'" type="image/png" alt="'+img_alt+'" title="'+img_alt+'" src="'+img_src+'" />'
    return ret
}

function getSketchpadPost () {
    
    return document.getElementById('sketchpad-post').value+' '+getBase64PNGImage ()
}


