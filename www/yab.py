import uwsgi
import select
from sys import stderr
from datetime import datetime
from time import time, ctime, sleep
from random import getrandbits

from Yab import Yab

def app(env, start_response):

    try:

        uwsgi.websocket_handshake(env['HTTP_SEC_WEBSOCKET_KEY'], env.get('HTTP_ORIGIN', ''))
        websocket = uwsgi.connection_fd()

        yab = Yab()

        # Visitor cookie
        if 'HTTP_COOKIE' in env:
            
            cookies = env['HTTP_COOKIE'].split(';')
            
            try:
            
                for c in cookies:
                    
                    if c.split('=')[0].strip() == 'VID':
            
                        vid = c.split('=')[1].strip()

            except IndexError as e:
                
                print(str(e), file=stderr)

        else:    
        
            # The visitorID (VID) (also referred as userID) is a random 512 long hexadecimal number
            # The cookie is set to expire in about 100 years
            vid = '%032x' % getrandbits(2048)
            expire = ctime(time()+3214080000)
            # Cookie is send to the client via the socket
            uwsgi.websocket_send('C:'+vid+'\u0000'+expire)
            
            
        yab.userID = vid    

        while True:

            ready = select.select([websocket], [], [], 1)

            if not ready[0]:
                
                uwsgi.websocket_recv_nb()
                
            for fd in ready[0]:
                
                if fd == websocket:
                    
                    try:
                        
                        msg = uwsgi.websocket_recv_nb()
                        
                    except IOError as e:
                        
                        print(str(e), file=stderr)
                        return ''
                    
                    msg = msg.decode('utf-8')
                    
                    # Client sends a new post    
                    if msg[0:2] == 'N:':

                        post = msg[2:].split('\u0000',2)
                        
                        if not post[2] or len(post[2]) > 131072:
                            
                            continue
                            
                        nor  = datetime.strftime(datetime.now(),'%Y%m%d%H%M%S')
                        post.insert(0, nor)
                        yab.addPost(post)

                    # Client adds/updates its username
                    elif msg[0:2] == 'U:':
                    
                        userid = msg[2:].split('\u0000',1)[0].strip()
                        username = msg[2:].split('\u0000',1)[1].strip()
                        if not username or not yab.isValidUsername(username):
                              
                            uwsgi.websocket_send('E:Invalid username: "'+username+'"')
                            continue
                            
                        elif yab.usernameExists(username, userid):
                            
                            uwsgi.websocket_send('E:Username "'+username+'" already registred')
                            continue 

                        yab.updateUser(userid, username)

                    # Client asks for its profile
                    elif msg[0:2] == 'W:':
                    
                        username     = yab.getUser(msg[2:])
                        permissions  = yab.getPermissions(msg[2:])
                        lastNotified = yab.getLastNotified()
                        
                        if not username:
                            
                            continue

                        uwsgi.websocket_send('Y:'+username+'\u0000'+str(permissions)+'\u0000'+str(lastNotified))
                        
                    # Client sends a query
                    elif msg[0:2] == 'Q:':
                        
                        l_query  = msg[2:].split('\u0000')
                        username = yab.getUser(l_query[0])
                        query_id = l_query[1]
                        cmd      = l_query[2]
                        args     = l_query[3:]
                        
                        ret = yab.query(cmd,args)
                        
                        uwsgi.websocket_send('R:'+username+'\u0000'+query_id+'\u0000'+ret[0]+'\u0000'+'\u0000'.join(ret[1])) 
                        
            try:
                
                # Server sends posts (if any) to the client and sleeps 300 ms
                for p in reversed(yab.getPosts()):
            
                        s_p = '\u0000'.join(map(str,p))
                        uwsgi.websocket_send('P:'+s_p)
                        
                sleep(0.3)

            except TypeError as e:
                
                print(str(e), file=stderr)

    except OSError as e:
            
        print(str(e), file=stderr)
