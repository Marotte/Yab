var host = 'yab.oxyure.com'
var ws = new WebSocket('wss://'+host+'/ws')
var highlight_color = 'yellow'
var visible_img = false


/* Do NOT edit after this line unless you really know what’s you’re doing. */
var username = ''
var capabilities = ''

document.addEventListener("DOMContentLoaded", function() {
  
    document.getElementById('loading-status').innerHTML = 'Loading…'
    setView()
    setTimeout(getUsername,260)
    setTimeout(Images,1000)
    setTimeout(completeView,1800)
    sketchpadInit()
    document.getElementById('loading-status').innerHTML = 'Connecting to '+host+'…'
})

if (document.cookie.replace(/(?:(?:^|.*;\s*)visible_img\s*\=\s*([^;]*).*$)|^.*$/, "$1") == 'true') {
    
    visible_img = true
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function insertAtCursor(myField, myValue) {

    if (myField.selectionStart || myField.selectionStart == '0') {
        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        myField.value = myField.value.substring(0, startPos)
            + myValue
            + myField.value.substring(endPos, myField.value.length);
    } else {
        myField.value += myValue
    }
}

function parentMessage (id,show) {
    /* Show parent message when the user moves the mouse over a post reference */
    
    if (show) {
    
        try {

            mv = document.getElementById('message-view')
            mv.innerHTML = document.getElementById('lid'+id).outerHTML
            mv.style.visibility = 'visible'
            
        } catch (e) { void null }
    
    } else {
        
        document.getElementById('message-view').innerHTML = ''
        document.getElementById('message-view').style.visibility = 'hidden'
        
    }
}

function setVID (vid, expire) {
    /* Set the userID cookie */
    
    document.cookie = 'VID='+vid+'; expires='+expire
}

function humanHorloge (horloge, short = false) {
    /* Take a "norloge" time representation and return a more human compatible format : HH:MM:SS if there is no ambiguity, pseudo ISO 8601 else. */
    
    var yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    
    ss = horloge.charAt(horloge.length - 2)+horloge.charAt(horloge.length - 1)
    mm = horloge.charAt(horloge.length - 4)+horloge.charAt(horloge.length - 3)
    hh = horloge.charAt(horloge.length - 6)+horloge.charAt(horloge.length - 5)
    dd = horloge.charAt(horloge.length - 8)+horloge.charAt(horloge.length - 7)
    MM = horloge.charAt(horloge.length - 10)+horloge.charAt(horloge.length - 9)
    yy = horloge.charAt(horloge.length - 14)+horloge.charAt(horloge.length - 13)+horloge.charAt(horloge.length - 12)+horloge.charAt(horloge.length - 11)

    d = new Date();
    d.setHours(hh);
    d.setMinutes(mm);
    d.setSeconds(ss);
    d.setDate(dd);
    d.setMonth(MM - 1);
    d.setFullYear(yy);
    
    if (d < yesterday && short == false) {
                
        return (yy+'-'+MM+'-'+dd+'T'+hh+':'+mm+':'+ss);

    } else {

        return (hh+':'+mm+':'+ss);
    }
}

function getVisitor () {
    /* Get our visitorID from our cookie */
    
    try {
        
        var ret = '';
        cookies = document.cookie.split(';');
        
        for (var i=0; i < cookies.length; i++) {
            
            c_name = cookies[i].split('=')[0].trim();
            c_val = cookies[i].split('=')[1].trim();

            if (c_name == 'VID') {
                
                ret = c_val;
            }
        }
        
    } catch (e) {
        
        ret = '-';
    }
    
    return ret;
}

function getUsername () {
    /* Send a message to the server asking for our registred username */
  
    send('W:'+getVisitor());
}

function refOnClick (id) {
    
    id = id.replace('mid','@')
    id = id.replace('hid','@')
    insertAtCursor(document.getElementById('post-input'), id+' ');
    document.getElementById('post-input').focus();
}



function process (message, sender, postId, norloge) {
    /* Transform a raw message we receive from the server to some useful HTML to display */
    
    var hrefRE = new RegExp('(@[0-9]{1,16})','g')
    var mrefRE = new RegExp('(@[A-zÀ-ÿ][A-zÀ-ÿ0-9_-]{1,63})','g')
    
    var span = document.createElement('span')
    span.setAttribute('class','message')
    span.innerHTML = message
    
    var notification = null
    
    // horloge ref
    // <span id="hid123" class="href hid123"> </span>
    span.innerHTML = span.innerHTML.replace(hrefRE, function(match) {
        
            id = match.replace('@','hid')
            return '<span id="'+id+'" class="href '+id+'">'+match+'</span>'
        
        })

    // mussel ref
    // <span id="mid123" class="mref mid123"> </span>
    span.innerHTML = span.innerHTML.replace(mrefRE, function(match) {
        
            id = match.replace('@','mid')
            
            if (username == match.substr(1)) { notifyUser(message, sender, postId, norloge) }
            
            return '<span id="'+id+'" class="mref '+id+'">'+match+'</span>'
        })



    span.addEventListener('mouseover', function (e) {

        try {
            
            c = e.target.firstChild.textContent.replace('@','')
            parentMessage(c,true,e.clientX,e.clientY)
            highlight(e.target.getAttribute('class').split(' ')[1],true) 
            
            } catch (e) { void null }
     
        })
        
    span.addEventListener('mouseout', function (e) {
                
            try { highlight(e.target.getAttribute('class').split(' ')[1],false); parentMessage(c,false) } catch (e) { void null } 
            
                
        })
        
    span.addEventListener('click', function (e) {
                
                try { refOnClick('@'+e.target.getAttribute('class').split(' ')[1].substr(3)) } catch (e) { void null }
                
        })
    
    return [span, null]
}

function addPost (post) {
    /* Add a received post to the page */
    
    var newline = document.createElement('li')
    var horloge = document.createElement('span')
    var post = post.split('\x00',5)
    var lid  = 'lid'+post[0]
    var hid   = 'hid'+post[0]
    var mid  = 'mid'+post[0]

    newline.setAttribute('id',lid)
    newline.setAttribute('class',lid)
    horloge.setAttribute('id',hid)
    horloge.setAttribute('title',humanHorloge(post[1]))
    horloge.setAttribute('class','horloge')
    horloge.innerHTML = humanHorloge(post[1], short = true)
    newline.appendChild(horloge)
    
    horloge.addEventListener('mouseover', function (e) {
                
                highlight(e.target.getAttribute('id').replace('hrid','hid'),true)
                
        })
        
    horloge.addEventListener('mouseout', function (e) {
                
                highlight(e.target.getAttribute('id').replace('hrid','hid'),false)
                
        })
        
    horloge.addEventListener('click', function (e) {
                
                refOnClick(e.target.getAttribute('id'))
                
        })
         
    var login = document.createElement('span')
    login.setAttribute('id',mid)
    login.setAttribute('title',post[2].trim())
    login.setAttribute('class','login')
    login.innerHTML = post[3]
    newline.appendChild(login)

    login.addEventListener('click', function (e) {
                
                console.log(e.target.firstChild.textContent)
                refOnClick('@'+e.target.firstChild.textContent)
        })

    message = process(post[4],post[3],post[0],post[1])[0]
    message.setAttribute('id','po'+post[0])
    newline.appendChild(message)
    document.getElementById('post-list').appendChild(newline);
    scrollDown();
} 

function editImage (image) {
    /* Show the sketchpad */
    document.getElementById('sketchpad-btn').click()
    ctx.drawImage(image,0,0)
}

function Images () {
    /* Hide or show totoz and drawings, and add a listener to make them editable if clicked */
    var images = document.getElementsByClassName('totoz')
    
    for (var i = 0; i < images.length;i++ ) {

        if (visible_img) {
            
            images[i].style.display = 'true'
            images[i].addEventListener('click', function (e) {
            editImage (e.target)
            })
        }
        
        else if (!visible_img) { 

            images[i].style.display = 'none'
        }
    }
    
    images = document.getElementsByClassName('drawing')
    
    for (var i = 0; i < images.length;i++ ) {

        if (visible_img) {
            
            images[i].style.display = 'true'
            images[i].addEventListener('click', function (e) {
            editImage (e.target)
            })
        }
        
        else if (!visible_img) { 

            images[i].style.display = 'none'
        }
    }
}

function processReply(type, records) {
    /* Process reply to query */
    
    if (type == 'userList') {
        
        var user_matrix = document.getElementById('user-matrix')        
        var th = document.createElement('tr')
        th.innerHTML = '<th>Username</th><th>Capabilities</th><th>Last notified</th>'
        user_matrix.appendChild(th)
        
        for (var i=0;i < records.length;i++) {

            var tr = document.createElement('tr')
            var user = records[i].split(':')
            var td1 = document.createElement('td')
            td1.innerHTML = user[0]

            td1.onclick = function (e) {
                
                document.getElementById('user-editor-username').innerHTML = e.target.innerHTML
                document.getElementById('user-editor').style.display = 'block'
                
            }
            
            tr.appendChild(td1)
            var td2 = document.createElement('td')
            td2.innerHTML = user[1]
            tr.appendChild(td2)
            var td3 = document.createElement('td')
            td3.innerHTML = user[2]
            tr.appendChild(td3)
            user_matrix.appendChild(tr)
        }
        
        
    }
    
    
}

ws.onmessage = function (event) {
    /* Message received. */
   
    console.log('← '+event.data)
    
    /* We’re asked to set the VID cookie */
    if (event.data.substr(0,2) == 'C:') {
        
        [vid, expire] = event.data.substr(2).split('\x00',2)
        setVID(vid, expire);
        
    }
    
    /* Message is a post */
    else if (event.data.substr(0,2) == 'P:') {
        
        addPost(event.data.substr(2));
        document.getElementById('post-input').focus()
        Images()
        
    }
    
    /* Message is our profile */
    else if (event.data.substr(0,2) == 'Y:') {
        
        [username, capabilities, lastNotified] = event.data.substr(2).split('\x00',3)
    }
    
    /* Message is an error message */
    else if (event.data.substr(0,2) == 'E:') {
        
        window.alert(event.data.substr(2))
    }
    
    /* Message is a response to a query */
    else if (event.data.substr(0,2) == 'R:') {
        
        l_res = event.data.substr(2).split('\x00')
        if (l_res[0] !== username) return false
        
        processReply (l_res[2],l_res.slice(3))
    }
}

function refresh() {
    
    window.location.assign('https://'+host);
}

ws.onclose = function (event) {

    refresh();
}

ws.onmerror = function (event) {

    window.alert(event.data);
    window.location.assign('https://'+host);
}

function send (data) {
    /* Send data to the server */
    
    console.log('→ '+data);
    ws.send(data);
}

function highlight (cl, state, color) {
    
    if (color === undefined) {
        
        color = highlight_color;
        
    }
    
    elms = document.getElementsByClassName(cl);
    
    if (state) {
    
        for (var i = 0; i < elms.length; i++) {
            
            elms[i].style.backgroundColor = color;
            
        }
        
    } else {
        
        for (var i = 0; i < elms.length; i++) {
            
            elms[i].style.backgroundColor = 'transparent';
            
        }
    }
}

function setListeners () {

    document.getElementById('post-input')
        .addEventListener('keyup', function(event) {
        event.preventDefault();
        if (event.keyCode == 13 || event.which == 13) {
            document.getElementById('post-btn').click()
        }
    })
    
    
    document.getElementById('post-input')
        .addEventListener('keydown', function(event) {
        if (event.keyCode == 35 || event.which == 35) {
            scrollDown()
        }
    })
    
    document.getElementById('post-input')
        .addEventListener('keydown', function(event) {
        if (event.keyCode == 36 || event.which == 36) {
            scrollUp()
            console.log('hop')
        }
    })

    document.getElementById('post-input')
        .addEventListener('keydown', function(event) {
        if (event.keyCode == 33 || event.which == 33) {
            document.getElementById('post-list').scrollTop -= 300
        }
    })

    document.getElementById('post-input')
        .addEventListener('keydown', function(event) {
        if (event.keyCode == 34 || event.which == 34) {
            document.getElementById('post-list').scrollTop += 300
        }
    })
    
    document.getElementById('post-btn')
        .addEventListener('click', function (e) {
            
            post(document.getElementById('post-input').value)
            document.getElementById('post-input').value = ''
            
        })
 
    
    document.getElementById('hide-imgs-btn')
        .addEventListener('click', Images)
        
        
    document.getElementById('sketchpad-color')
        .addEventListener('change', function (e) {
        document.cookie = 'yab_sketchpad_color='+e.target.value
        })
        
    document.getElementById('sketchpad-pensize')
        .addEventListener('change', function (e) {
        document.cookie = 'yab_sketchpad_pensize='+e.target.value
        })
        
    document.getElementById('sketchpad-alpha')
        .addEventListener('change', function (e) {
        document.cookie = 'yab_sketchpad_alpha='+e.target.value
        })
}

   

function setActions (host) {
    
    var post_input              = document.getElementById('post-input')
    var profile                 = document.getElementById('profile-edit')
    var profile_btn             = document.getElementById('profile-btn')
    var user_id                 = document.getElementById('user-id')
    var user_name               = document.getElementById('user-name')
    var user_capabilities       = document.getElementById('user-capabilities')
    var sketchpad_drawtype      = document.getElementById('sketchpad-drawtype')
    var sketchpad_color         = document.getElementById('sketchpad-color')
    var sketchpad_pensize       = document.getElementById('sketchpad-pensize')
    var sketchpad_alpha         = document.getElementById('sketchpad-alpha')
    var close_profile           = document.getElementById('profile-close')
    var cancel_profile          = document.getElementById('profile-cancel')
    var hide_imgs_btn           = document.getElementById('hide-imgs-btn')
    var sketchpad_draw          = document.getElementById('sketchpad-draw')
    var sketchpad_btn           = document.getElementById('sketchpad-btn')
    var close_sketchpad         = document.getElementById('close-sketchpad')
    var cancel_sketchpad        = document.getElementById('cancel-sketchpad')
    var user_editor             = document.getElementById('user-editor')
    var close_usereditor        = document.getElementById('usereditor-close')
    var cancel_usereditor       = document.getElementById('usereditor-cancel')


    // Show profile
    profile_btn.onclick = function() {
        
        user_name.value               = username
        user_id.innerHTML             = '<b>Visitor ID:</b> '+getVisitor()
        user_capabilities.innerHTML   = '<b>Capabilities:</b> '+capabilities
        sketchpad_drawtype.value      = getCookie('yab_sketchpad_drawtype')
        sketchpad_color.value         = getCookie('yab_sketchpad_color')
        sketchpad_pensize.value       = getCookie('yab_sketchpad_pensize')
        sketchpad_alpha.value         = getCookie('yab_sketchpad_alpha') 
        profile.style.display         = 'block';
    }
    
    // "close" is a "save & close"
    close_profile.onclick = function() {
        
        if (user_name.value.trim() != '') {
        
            document.cookie = 'yab_sketchpad_drawtype='+sketchpad_drawtype.value
            document.cookie = 'yab_sketchpad_color='+sketchpad_color.value
            document.cookie = 'yab_sketchpad_pensize='+sketchpad_pensize.value
            document.cookie = 'yab_sketchpad_alpha='+sketchpad_alpha.value
            send('U:'+getVisitor()+'\x00'+user_name.value.trim())
            document.getElementById('post-input').focus()
            
        }
        
        profile.style.display = 'none'
        setTimeout(refresh, 1000)
    }

    cancel_profile.onclick = function(event) {

            profile.style.display = 'none'
            post_input.focus()
        }

    // When the user clicks anywhere outside of the profile modal, close it without doing anything.
    window.onclick = function(event) {
        if (event.target == profile) {
            profile.style.display = 'none'
            post_input.focus()
        }
    }
    
    // Show sketchpad
    sketchpad_btn.onclick = function() {
        

        document.getElementById('sketchpad').setAttribute('width',1200)
        document.getElementById('sketchpad').setAttribute('height',200)
        
        sketchpad_drawtype.value      = getCookie('yab_sketchpad_drawtype')
        sketchpad_color.value         = getCookie('yab_sketchpad_color')
        sketchpad_pensize.value       = getCookie('yab_sketchpad_pensize')
        sketchpad_alpha.value         = getCookie('yab_sketchpad_alpha')
        sketchpad_draw.style.display  = 'block'
        document.getElementById('sketchpad-post').value=''
        document.getElementById('drawing-title').value=''
    }
    
    // "close" is a "post & close"
    close_sketchpad.onclick = function() {

        document.getElementById('post-input').focus()
        HTML = getSketchpadPost()
        post(HTML)
        sketchpad_draw.style.display = 'none'
    }
    
    // Close the sketchpad without posting the drawing
    cancel_sketchpad.onclick = function() {

        document.getElementById('post-input').focus()
        sketchpad_draw.style.display = 'none'
    }

    // Hide/show images
    hide_imgs_btn.onclick = function() {

        visible_img = !visible_img;
        
        if (!visible_img) {
            
            document.cookie = 'visible_img=false;'
            
            
        } else {
            
            document.cookie = 'visible_img=true;'
            document.getElementById('hide-imgs-btn').setAttribute('title','Hide images')

        }

        refresh();
    }
    
    // Close/Cancel User edition
    close_usereditor.onclick = function() {

        document.getElementById('post-input').focus()
        user_editor.style.display = 'none'
    }
    cancel_usereditor.onclick = function() {

        document.getElementById('post-input').focus()
        user_editor.style.display = 'none'
    }

}

function scrollDown () {
    
    var postList = document.getElementById('post-list');
    postList.scrollTop = postList.scrollHeight;
}

function scrollUp () {
    
    var postList = document.getElementById('post-list');
    postList.scrollTop -= postList.scrollHeight;
}

function setView () {
    /* Initialize different things, called on page load and at few other occasions too. */
    
    var h = parseInt(window.innerHeight * 92 / 100)
    document.getElementById('post-list').style.height = h+'px'
    document.getElementById('post-input').focus()
    scrollDown()
    setListeners()
    setActions()
    var scale = 'scale(1)'
    document.body.style.webkitTransform =  scale
    document.body.style.transform = scale
    if (visible_img) {
    
        document.getElementById('hide-imgs-btn').setAttribute('title','Hide images')
    
    } else {
        
        document.getElementById('hide-imgs-btn').setAttribute('title','Show images')
    }
    
    document.getElementById('message-view').style.width = (window.innerWidth - 100)+'px'

}

function completeView () {
    /* Add or modify some elements on the page according to user capabilities.
     * It’s called 3 seconds after page load to ensure the server already replied back
     * to provide username and associated capabilities */

    if (!username || !capabilities) {
        
        document.getElementById('loading-status').innerHTML = ''
        document.getElementById('splashscreen').style.display = 'none'
        
        return null
        
    }

    if (capabilities.indexOf('modifyCapabilities') !== -1) {
        
        document.getElementById('loading-status').innerHTML = 'Loading capabilities…'
        var new_capabilities = [] // will store requested capabilities to send back to the server
        
        console.log(username+' '+capabilities)
        document.getElementById('users-btn').style.display = 'inline'
        document.getElementById('users-btn').addEventListener('click', function (e) {
            
            document.getElementById('users-edit').style.display = 'block'
            document.getElementById('user-matrix').innerHTML = ''
            query('listUsers',[])
            
            // Close and save
            document.getElementById('users-close').onclick = function() {
           
                document.getElementById('post-input').focus()
                query('modifyCapabilities', new_capabilities)
                document.getElementById('users-edit').style.display = 'none'
            }
            
            // Close the users edition view without modification
            document.getElementById('users-cancel').onclick = function() {
           
                document.getElementById('post-input').focus()
                document.getElementById('users-edit').style.display = 'none'
            }
        })
    }

    // The board is ready → remove the splashscreen
    document.getElementById('loading-status').innerHTML = ''
    document.getElementById('splashscreen').style.display = 'none'

}

function post (message) {
    /* Send a new message to the server. */
    send('N:'+navigator.userAgent+'\x00'+getVisitor()+'\x00'+message);
}

function query (cmd, params) {
    /* Send a query to the server */
    send('Q:'+getVisitor()+'\x00'+String(Math.random()).substr(2)+'\x00'+cmd+'\x00'+params.join('\x00'));
    
}


function notifyUser(message, sender, id, norloge) {

  console.log(message+'|'+sender+'|'+id+'|'+norloge)
  
  if (!("Notification" in window)) { void null }
  
  else if (id <= lastNotified) { void null }

  else if (Notification.permission === "granted") {

    var notification = new Notification(humanHorloge(norloge)+' '+sender, {body: message, tag: id})
    query('setLastNotified',[id])
  }

  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {

      if(!('permission' in Notification)) {
        Notification.permission = permission
      }

      if (permission === "granted") {
        var notification = new Notification(humanHorloge(norloge)+' '+sender, {body: message, tag: id})
        query('setLastNotified',[id])
      }
    })
  }
}
