import uwsgi
import select
import sqlite3
import re
from time import sleep
from hashlib import sha256
from os import path
from sys import stderr
from html import escape, unescape
from base64 import b64encode
from requests import get

class Yab:
    """The Yab object handles database operations, text transformation and user requests."""
    
    dbfile = 'yab.db'
    dbsize = 200
    totozBackend  = 'https://totoz.eu/img/'
    re_totoz      = re.compile('\[:([ _:A-Za-z0-9-]+)]')
    re_url        = re.compile(r'(http|ftp|https)://.*')
    re_bold       = re.compile(r' \*(.*)\* ')
    re_italic     = re.compile(r' /(.*)/ ')
    re_stroke     = re.compile(r' -(.*)- ')
    re_code       = re.compile(r' `(.*)` ')
    re_underline  = re.compile(r' _(.*)_ ')
    re_username   = re.compile(r'^[A-zÀ-ÿ][A-zÀ-ÿ0-9_-]{1,63}$')


    def __init__(self):
        """Create the database for the backend if not exists."""
        
        self.lastID = 0
        self.userID = None
        
        if not path.exists(self.dbfile):
        
            self.db  = sqlite3.connect(self.dbfile)
            self.dbc = self.db.cursor()
            self.dbc.execute('PRAGMA foreign_keys_ = ON')
            self.dbc.execute('CREATE TABLE post (id INTEGER PRIMARY KEY, time INT, info TEXT, login TEXT, message TEXT)')
            self.dbc.execute('CREATE TRIGGER trimer AFTER INSERT ON post \
                              BEGIN DELETE FROM post WHERE id < ((SELECT max(id) FROM post) - '+str(self.dbsize)+'); END')
            
            self.dbc.execute('CREATE TABLE user (hash TEXT UNIQUE, username TEXT UNIQUE, last_notified INTEGER)')
            
            self.dbc.execute('CREATE TABLE capability (capability TEXT PRIMARY KEY, description TEXT)')
            self.dbc.execute('CREATE TABLE permission  (id INTEGER PRIMARY KEY, userhash TEXT, capability TEXT, \
                              FOREIGN KEY(userhash) REFERENCES user(hash), FOREIGN KEY(capability) REFERENCES capability(capability))')
            self.db.commit()
            self.dbc.execute('INSERT OR IGNORE INTO capability (capability, description) VALUES ("modifyCapabilities", "Add/Remove user capabilities.")')
            self.dbc.execute('INSERT OR IGNORE INTO capability (capability, description) VALUES ("listUsers", "List users.")')
            self.dbc.execute('INSERT OR IGNORE INTO capability (capability, description) VALUES ("resetBackend", "Remove all posts from the backend.")')
            self.db.commit()

        else:
            
            self.db = sqlite3.connect(self.dbfile)
            self.dbc = self.db.cursor()
        
    def hashForUsername (self, userid):
        """Return a default username for given a userID. It’s a sha256 hash with the first character replaced by 'V', as username must not start with a digit."""
        
        lehash = sha256(userid.encode('utf-8')).hexdigest()
        # Replace first character with 'V' (usernames must not start with a digit)
        lehash = 'V'+lehash[1:]
        return lehash

    def getUser(self, userid):
        """Return the registred username for a given userID, return the default username if there is no such user in the database."""
        
        lehash = self.hashForUsername(userid)
        username = self.dbc.execute('SELECT username FROM user WHERE hash = ?', (lehash,)).fetchone()
        
        if username:

            return username[0]
            
        return lehash 

    def usernameExists(self, username, userid):
        """Check if a username exists (ie: is registred in the database)."""
        
        lehash = self.hashForUsername(userid)
        
        if self.dbc.execute('SELECT username FROM user WHERE username = ? AND hash != ?', (username.strip(), lehash)).fetchone():
            
            return True
            
        else:
            
            return False

    def isValidUsername (self, username):
        """Check if a given string is a valid username."""
        if self.re_username.match(username):
            
            return True
            
        else:
            
            return False    

    def updateUser(self, userid, username):
        """Add or update userID ↔ username association."""

        lehash = self.hashForUsername(userid)
        username = username.strip()
        if not self.isValidUsername(username):
            return False

        self.dbc.execute('INSERT OR IGNORE INTO user (hash, username) VALUES (?,?)', (lehash, username[0:64]))
        self.dbc.execute('UPDATE OR IGNORE user SET username = ? WHERE hash = ?', (username[0:64], lehash))
        self.db.commit()
        return True
    
    def getPosts(self):
        """Return posts newer than our lastID and update lastID."""
        self.dbc.execute('SELECT * FROM post WHERE id > ? ORDER BY id DESC', (self.lastID,))
        posts = self.dbc.fetchall()
        
        try:
            
            self.lastID = posts[0][0]
            
        except IndexError:
            
            pass  
             
        return posts
        
    def getPermissions(self, userid):
        """Return permissions (capabilities) for a given userID as a string."""
        
        self.dbc.execute('SELECT capability FROM permission WHERE userhash = ?', (self.hashForUsername(userid),))
        
        perms = self.dbc.fetchall()
        return ','.join([x[0] for x in perms])
        
    def sanitize(self, wild_shit):
        """Take a random string and produce YBML (Yab Board Markup Language)."""
        
        r = escape(re.sub('\s+',' ',wild_shit.replace('\u00A0',' ').replace('\t',' ')))

        ok_tags = ['b','i','s','code','tt']
        tokens = []
        
        for t in ok_tags:
            
            r = r.replace('&lt;'+t+'&gt;','<'+t+'>')
            r = r.replace('&lt;/'+t+'&gt;','</'+t+'>')
            
        for token in r.split():

            nt = token

            if self.re_url.match(token):
                    
                    nt = '<a href="'+token.replace('&amp;','&')+'">&lsqb;url&rsqb;</a>'
                    tokens.append(nt)

            else:
                
                tokens.append(nt)

        ret = ' '.join(tokens)

        ret = re.sub(self.re_bold,r' <b>\1</b> ',' '+ret+' ')
        ret = re.sub(self.re_italic,r' <i>\1</i> ',' '+ret+' ')
        ret = re.sub(self.re_stroke,r' <s>\1</s> ',' '+ret+' ')
        ret = re.sub(self.re_code,r' <code>\1</code> ',' '+ret+' ')
        ret = re.sub(self.re_underline,r' <u>\1</u> ',' '+ret+' ')
        
        # Sketeched image
        ret = ret.replace('&lt;img class=&quot;drawing&quot; ','<img class="drawing" ')
        ret = ret.replace('=&quot;','="')
        ret = ret.replace('&quot; ','" ')
        ret = ret.replace(' /&gt;',' />')
        
        # Totoz
        try:
            
            for m in self.re_totoz.findall(ret):
                
                b64image = b64encode(get(self.totozBackend+m).content).decode('utf-8')
                totoz_tag = '<img class="totoz" title="'+m+'" alt="'+m+'" src="data:image/png;base64,'+b64image+'">'
                ret = ret.replace('[:'+m+']', totoz_tag)
                
        except TypeError:
            
            pass

        return ret.strip()

    def addPost(self, post):
        """Add a post to the database after sanitization of "info" and "message" and addition of the username."""
        
        if not post[3].strip():
            
            return False
        
        post[1] = self.sanitize(post[1])
        post[2] = self.getUser(post[2])
        post[3] = self.sanitize(post[3])
        
        self.dbc.execute('INSERT INTO post (time,info,login,message) VALUES (?,?,?,?)', post)
        self.db.commit()
        return True

        
    def getUsers(self):
        
        return self.dbc.execute('SELECT username, capability, last_notified FROM user LEFT JOIN permission ON user.hash=permission.userhash').fetchall()

    def setLastNotified(self, post_id):
        
        lehash = self.hashForUsername(self.userID)
        self.dbc.execute('UPDATE user SET last_notified = ? WHERE hash = ?',(post_id, lehash))
        self.db.commit()

    def getLastNotified(self):
        
        lehash = self.hashForUsername(self.userID)
        ln = self.dbc.execute('SELECT last_notified FROM user WHERE hash = ?', (lehash,)).fetchone()
        
        if ln:
            
            return ln[0]
            
        else:
        
            return 0

    def userCan(self, capability):
        
        perms = self.getPermissions(self.userID).split(',')
        
        if capability in perms:
            
            print('OK   '+self.userID+' has been granted "'+capability+'"\n', file=stderr)
            return True
        
        print('!NOK '+self.userID+' has been denied "'+capability+'"\n', file=stderr)
        return False

    def query(self, cmd, args):
        
        ret = ('',[])
        
        if cmd == 'listUsers':
            
            if not self.userCan('listUsers'):
                
                return ('Error', ['Unauthorized'])
            
            users = []
            for u in self.getUsers():
                
                try:
                    
                    users.append(':'.join(map(str,u)))
                    
                except TypeError:
                    
                    pass    
            
            return ('userList',users)
        
        if cmd == 'setLastNotified' and len(args) == 1:
            
            self.setLastNotified(args[0])
            
        return ret    
            
         
