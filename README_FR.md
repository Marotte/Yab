# YAB

YAB (ou Yab) est un outil d’optimisation de la communication entre personnes, il promeut le travail en équipe et la méthode agile. Il augmente ainsi la productivité globale de votre organisation.

On peut le décrire comme une application web entre le salon de discussion et le micro-blogging. Il y a aussi un tableau blanc, parce que parfois un dessin vaut mieux qu’un long discours…

Yab est une application moderne, à l’état de l’art. Il dépend de Python3, SQLite, [uWSGI](https://uwsgi-docs.readthedocs.io/en/latest/) et Nginx.

La partie cliente est en Javascript pur, elle n’utilise pas de bibliothèque externes telles que AngularJS, Node.js, jQuery, etc… Le code serveur devrait fonctionner avec Python ≥ 3.4 and uWSGI ≥ 2.0.

## Installation

Vous devez modifier/créer trois fichiers pour configurer une instance de Yab :

### Le fichier uWSGI : _yab.ini_

Le fichier doit être placé sous _/etc/uwsgi/apps-available_ avec un lien symbolique dans _/etc/uwsgi/apps-enabled_ pour l’activer.

Vous devez au minimum modifier le paramètre 'chdir' selon vos besoins.

### Le fichier d’hôte virtuel pour Nginx
 
_yab.nginx.snippet_ n’est pas un fichier vhost entier mais devrait vous permettre d’écrire le votre.

### Le fichier Javascript : _yab.js_
 
Il contient l’adresse du serveur de socket, vous devez modifier la variable 'host' (et éventuellement 'ws' selon votre configuration de Nginx)

    var host = 'yab.example.com'
    var ws = new WebSocket('wss://'+host+'/ws')

ce sont les deux premières lignes du script.

## Personalisation

YAB offre peu d’option de personnalisation, vous pouvez néanmoins éditer ces deux fichiers :

### yab.css

Pour changer des paramètres du client, comme les couleurs.

### index.html

Pour le titre de la page ou les mot-clés.

## Démonstration

Il y a une instance de démonstration à [oxyure.com](https://board.oxyure.com). Sentez-vous libre d’expérimenter, mais n’oubliez pas qu’il s’agit d’une version « beta », et que bon nombre de fonctionnalités ne sont pas encore implémentées. Manifestement elles ne l’ont jamais été et ne le seront probablement jamais.

Le logiciel est parfait tel qu’il est. Les fonctionnalités mal branlées ont toutes pour but de nous rappeller que dans nos imperfections se tient notre humanité.