#!/bin/sh
# Restart the Yab WSGI and "tail feed" its log file to stdout.

kill -1 $(cat /var/run/uwsgi/app/yab/pid) &
tail -n90 -f /var/log/uwsgi/app/yab.log



